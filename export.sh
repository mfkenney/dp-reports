#!/bin/bash
#
# Export DP data as CSV. Requires csvkit.
#

[[ $# == 3 ]] || {
    echo "Usage: $(basename $0) pstart pend raw|processed" 1>&2
    exit 1
}

# Starting profile
PSTART=$1
PEND=$2
SENSORS=("ctd_1" "optode_1" "acm_1" "flntu_1" "flcd_1")

case "$3" in
    processed)
        BASE="export_processed"
        tables=()
        for s in "${SENSORS[@]}"; do
            tables+=("${s}_processed")
        done
        ;;
    *)
        BASE="export_raw"
        tables=("${SENSORS[@]}")
        ;;
esac
mkdir -p "$BASE"

# Database URL
db="sqlite:///rawdata.db"

sql2csv -H --db "$db"\
        --query "select start*1000000,end*1000000,pnum,mode from profiles where (pnum between $PSTART and $PEND) and mode in ('up', 'down')" |\
    tr "," " " |\
    while read t0 t1 p m; do
        for tab in "${tables[@]}"; do
            file="$BASE/${tab}_$(printf "%04d" $p)_${m}.csv"
            sql2csv --db "$db"\
                    --query "select * from $tab where timestamp between $t0 and $t1" > $file
            echo "Exported $file"
        done
    done
