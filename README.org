#+TITLE: Deep Profiler Data
#+OPTIONS: toc:nil H:4 timestamp:t ^:{} d:nil
#+LATEX_CLASS: mfk-org-article
#+LATEX_CLASS_OPTIONS: [11pt,letterpaper]
#+LATEX_HEADER: \fancyfoot[CE,CO]{Deep Profiler Data}

This directory contains two ZIP archives of datasets from the Slope
Base Deep Profiler. There is one dataset (file) for each
profile/sensor combination. The filenames use the following
convention:

#+BEGIN_EXAMPLE
<TABLE>_PPPP_<DIR>.csv
#+END_EXAMPLE

Where /<TABLE>/ is the database table name, /PPPP/ is the profile number,
and /<DIR>/ is the profile direction, '=up=' or '=down='.

** Raw Data

All datasets contain a /timestamp/ column with units of microseconds since
1/1/1970 UTC. This is a large value and requires a 64-bit integer for
storage.

*** Seabird 52-MP
The table name for this sensor is =ctd_1=.

#+ATTR_LATEX: :align |l|l|l|
| *Column* | *Description* | *Units* |
|----------+---------------+---------|
| condwat  | Conductivity  | mS/cm   |
| tempwat  | Temperature   | deg C   |
| preswat  | Pressure      | decibar |

*** FSI 3D-ACM-Plus-MMP
The table name for this sensor is =acm_1=.

#+ATTR_LATEX: :align |l|l|l|
| *Column* | *Description*           | *Units* |
|----------+-------------------------+---------|
| va       | Path velocity axis A    | cm/s    |
| vb       | Path velocity axis B    | cm/s    |
| vc       | Path velocity axis C    | cm/s    |
| vd       | Path velocity axis D    | cm/s    |
| hx       | Normalize magnetic flux |         |
| hy       | Normalize magnetic flux |         |
| hz       | Normalize magnetic flux |         |
| tx       | X tilt                  | degrees |
| ty       | Y tilt                  | degrees |

*** Aanderaa Optode  4831
The table name for this sensor is =optode_1=.

#+ATTR_LATEX: :align |l|l|l|
| *Column* | *Description*    | *Units* |
|----------+------------------+---------|
| doconcs  | Calibrated phase | degrees |
| t        | Temperature      | deg C   |


*** Wetlabs FLNTURTD
The table name for this sensor is =flntu_1=.

#+ATTR_LATEX: :align |l|l|l|
| *Column* | *Description*             | *Units*    |
|----------+---------------------------+------------|
| chlaflo  | Chlorophyll concentration | A/D counts |
| ntuflo   | Turbidty                  | A/D counts |

*** Wetlabs FLCDRTD
The table name for this sensor is =flcd_1=.

#+ATTR_LATEX: :align |l|l|l|
| *Column* | *Description*                    | *Units*    |
|----------+----------------------------------+------------|
| cdomflo  | Colored Dissolved Organic Matter | A/D counts |


** Processed Data

The data from some of the sensors has been processed using the
procedures outlined in the following Data Product Specifications:

  - /Data Product Specification for Practical Salinity/, OOI
    Document number 1341-00040
  - /Data Product Specification for Density/, OOI Document number
    1341-00050
  - /Data Product Specification for Oxygen Concentration from "Stable" Instruments/,
    OOI Document number 1341-00520

*** CTD
The table name for the processed data is =ctd_1_processed=.

#+ATTR_LATEX: :align |l|l|l|
| *Column* | *Description*   | *Units*  |
|----------+-----------------+----------|
| pracsal  | Salinity        | psu      |
| tempwat  | Temperature     | deg C    |
| preswat  | Pressure        | decibar  |
| density  | In-situ density | kg/m^3   |


*** Optode

The table name for the processed data is =optode_1_processed=.

#+ATTR_LATEX: :align |l|l|l|
| *Column* | *Description*                  | *Units*       |
|----------+--------------------------------+---------------|
| preswat  | Pressure                       | decibar       |
| doxygen  | Dissolved oxygen concentration | micromoles/kg |


*** FLNTURTD

The table name for the processed data is =flntu_1_processed=. This
data set was processed using the calibration coefficients from
Wetlabs.

#+ATTR_LATEX: :align |l|l|l|
| *Column* | *Description*             | *Units*          |
|----------+---------------------------+------------------|
| preswat  | Pressure                  | decibar          |
| chla     | Chlorophyll concentration | micrograms/liter |
| ntu      | Turbidity                 | NTU              |


*** FLCDRTD

The table name for the processed data is =flcd_1_processed=. This
data set was processed using the calibration coefficients from
Wetlabs.

#+ATTR_LATEX: :align |l|l|l|
| *Column* | *Description*      | *Units* |
|----------+--------------------+---------|
| preswat  | Pressure           | decibar |
| cdom     | CDOM concentration | ppb     |

*** ACM

The table name for the processed data is =acm_1_processed=. This is only
partially processed, in that the velocities have been converted from Beam
coordinates to Instrument coordinates (ijk). Conversion to geographic
coordinates (east, north, up) will need to wait for the post-recovery
compass calibration.

#+ATTR_LATEX: :align |l|l|l|
| *Column* | *Description*                 | *Units* |
|----------+-------------------------------+---------|
| preswat  | Pressure                      | decibar |
| vi       | Horizontal velocity component | cm/s    |
| vj       | Horizontal velocity component | cm/s    |
| vk       | Vertical velocity component   | cm/s    |
